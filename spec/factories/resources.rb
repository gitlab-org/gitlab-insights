FactoryBot.define do
  factory :resource do
    project

    trait :issue do
      resource_type :issue
      labels { ['issue_label'] }

      initialize_with { new(labels: labels) }
    end

    trait :merge_request do
      resource_type :merge_request
      labels { ['mr_label'] }

      initialize_with { new(labels: labels) }
    end

    trait :milestone do
      resource_type :milestone
    end

    trait :pipeline do
      resource_type :pipeline
    end
  end
end
