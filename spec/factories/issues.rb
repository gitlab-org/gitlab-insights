FactoryBot.define do
  factory :issue do
    project
    labels { ['issue_label'] }
    state 'opened'
    milestone { { title: 'milestone' } }
    author { { username: 'username' } }

    trait :open do
      state 'opened'
    end

    trait :closed do
      state 'closed'
    end

    trait :reopened do
      state 'reopened'
    end
  end
end
