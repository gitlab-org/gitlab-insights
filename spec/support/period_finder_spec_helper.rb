def assert_results(expected, actual, field)
  expected.each_with_index do |expected_element, i|
    expect(actual[i][field]).to eq(expected_element[field])
  end
end

def assert_results_included(expected, actual, field)
  expected.each_with_index do |expected_element, i|
    expect(actual).to include(expected_element)
  end
end
