require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'missed_deliverables field' do
    let(:view_string) { 'missed_deliverables' }

    it_behaves_like 'issuable_per_label view'
  end
end
