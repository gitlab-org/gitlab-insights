require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  let(:view_string) { 'average_issuables_merged_per_month' }
  let(:issuable_timestamp_field) { :merged_at }
  let(:query_period) { :month }
  let(:query_period_format) { "%B %Y" }

  context 'average_issuables_merged_per_month field' do
    context 'MRs' do
      context 'merged' do
        let(:issuable_status) { :merged }

        include_examples 'mr query' do
          include_examples 'average merged issuables per month'
        end
      end
    end
  end
end
