require 'rails_helper'

RSpec.describe GitlabInsights::ResourceCategorizer do
  let(:group) { create(:group) }
  let(:project) { create(:project, group: group) }

  context 'for issues' do
    let(:resource) { ApiResponses.issues.first.with_indifferent_access }
    subject { described_class.new(:issues, resource, project: project).process }

    it 'does nothing' do
      expect(subject). to eq(resource)
    end
  end

  context 'for merge requests' do
    let(:resource) { ApiResponses.merge_requests.first.with_indifferent_access }
    subject { described_class.new(:merge_requests, resource, project: project).process }

    context 'non-dev project' do
      it 'does nothing' do
        expect(subject). to eq(resource)
      end
    end

    context 'dev project' do
      let(:project) { create(:project, :dev, group: group) }

      it 'adds the security label' do
        expect(resource[:labels]).not_to include('security')

        expect(subject[:labels]).to include('security')
      end
    end
  end
end
