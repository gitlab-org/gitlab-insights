require 'rails_helper'

RSpec.describe GitlabInsights::ResourceTranslator do
  [:issues, :merge_requests].each do |resource_type|
    let(:group) { create(:group) }
    let(:project) { create(:project, group: group) }
    let(:resource) { ApiResponses.send(resource_type).first.with_indifferent_access }
    let(:resource_keys) { resource.keys.map(&:to_sym) }

    subject { described_class.new(resource_type, resource, project: project).process }

    context 'dev project' do
      let(:project) { create(:project, :dev, group: group) }

      it 'translates the username' do
        original_username = 'mark'
        expected_username = 'markglenfletcher'

        resource[:author][:username] = original_username

        expect(subject[:author][:username]).to eq(expected_username)
      end
    end

    it 'does not translate the username' do
      original_username = 'mark'

      resource[:author][:username] = original_username

      expect(subject[:author][:username]).to eq(original_username)
    end
  end
end
