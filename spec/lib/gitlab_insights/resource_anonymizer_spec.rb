require 'rails_helper'

RSpec.describe GitlabInsights::ResourceAnonymizer do
  [:issues, :merge_requests].each do |resource_type|
    let(:resource) { ApiResponses.send(resource_type).first.with_indifferent_access }
    let(:resource_keys) { resource.keys.map(&:to_sym) }
    let(:anon_fields) { resource_keys - described_class::WHITELISTED_ATTRIBUTES }

    subject { described_class.new(resource_type, resource).process }

    it 'deletes necessary fields' do
      anon_fields.each do |attr|
        expect(subject[attr]).to be_nil
      end
    end

    it 'copes when the field is missing' do
      resource.delete(anon_fields.first)

      subject
    end

    it 'keeps the whitelisted fields' do
      orig_resource = resource.dup.with_indifferent_access

      described_class::WHITELISTED_ATTRIBUTES.each do |field|
        expect(subject[field]).to eq(orig_resource[field])
      end
    end
  end
end
