## Data Sync

The dashboard is powered by data that is rebuilt periodically.

Graphs are built on the assumption that the dataset is brand new.

- Data is downloaded from the GitLab.com API
- Data is downloaded using a limited user account with no membership to the `gitlab-org` group or projects
 - This means that only publicly accessible data should ever enter the database. (no confidential issues)
- Data is anonymised against a whitelist of attributes before being persisted
- Once the data is downloaded, all current data is purged
 - This allows the sync to fail and run safely without affecting the current data and is free to run again later 
 - This ensures that the dashboard has minimal downtime whilst the new data is reimported, because the download operation can take a long time depending on the load on GitLab.com's API servers

Note: There is a WIP MR to introduce post populate actions that may change things

### User

The user account used for downloading the data is [@gitlab-insights-user](https://gitlab.com/gitlab-insights-user)

Additional data is available in the 1password Team Vault under `GitLab Insights Populate User`.

The access token is stored as a secret variable in this project for use in the pipeline.

### Production environment

Data for the production environment is rebuilt by a scheduled pipeline that currently runs twice per day (likely to move to once per day as workload increases).

### Review environment

Data for the review environment is rebuilt using a manual action and the data here can often run behind production
