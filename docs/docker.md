## Docker

This document details the Docker environment used to run the app in `production` and `review` environments

### Proxy

Requests are routed through a container acting as an automated proxy. Using [nginx-proxy](https://github.com/jwilder/nginx-proxy).

Containers where requests are to be routed should be started with a `VIRTUAL_HOST` environment variable, which must match the URL.

For example:

For a review app for a feature branch named `feature`, the environment URL that is generated will be:

`feature-review.quality-dashboard.gitlap.com`

This is the value that the `VIRTUAL_HOST` environment variable must be set to to route the requests through to that container.

### Database

The MongoDB database is running in a separate container exposing port 27017. The mongodb container has a hostname of `mongo` which is the name the other containers use to communicate with it.

Please see [mongoid.yml](/config/mongoid.yml) for more info.

Both the `review` and `production` databases are currently using the same container

### Networking

All containers are using the same network called `insights-net`

### Application containers

See the [Dockerfile](/Dockerfile) for how the app containers are built

All application containers must upon starting:
- Specify the `RAILS_ENV`
- Use the `insights-net` network
- Link the mongodb container

A production container will always be running `master`

Review apps will be spun up and down as feature branches are created and deleted

Containers are spun up to **populate** the `production` and `review` databases using the `latest` tag of their respective images. Therefore, the following tags must always be available to use:

- gitlab-insights-production:latest
- gitlab-insights-review:latest

Please see the `populate` stage in the [.gitlab-ci.yml](/.gitlab-ci.yml) for more info
