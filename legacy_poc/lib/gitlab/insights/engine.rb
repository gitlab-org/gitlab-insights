require 'active_support/all'

require_relative 'url_builders/url_builder'
require_relative 'series/issues_by_state'
require_relative 'models/issues_by_state'
require_relative 'network'
require_relative 'network_adapters/httparty_adapter'
require_relative 'repository'
require_relative 'repository_adapters/postgres'
require_relative 'repository_adapters/sqlite'
require_relative 'ui'

module Gitlab
  module Insights
    # This class is responsible for centralizing the instantiation of a network
    # and a repository adapter.
    # The network adapter retrieves the data while the repository adapter save
    # them in a database.
    class Engine
      attr_reader :host_url, :api_version, :per_page, :options

      def initialize(options:, network_adapter_class: Gitlab::Insights::NetworkAdapters::HttpartyAdapter)
        @options = options
        @network_adapter_class = network_adapter_class

        options.dry_run = true if ENV['TEST'] == 'true'

        assert_project_id!
        assert_token!
      end

      def perform
        if options.dry_run
          UI.debug "Performing a dry run.\n\n"
        else
          repository.install
        end

        Series::IssuesByState
          .new(network: network, repository: repository, options: options)
          .sync

        # Number of opened issues
        # Number of closed issues
        # Number of open issues per day
        # Number of ~"support request" open issues per day
      end

      private

      def assert_project_id!
        return if options.project_id

        raise ArgumentError, 'A project_id is needed (pass it with the `--project-id` option)!'
      end

      def assert_token!
        return if options.token

        raise ArgumentError, 'A token is needed (pass it with the `--token` option)!'
      end

      def network
        @network ||= Network.new(network_adapter, options)
      end

      def network_adapter
        @network_adapter ||= @network_adapter_class.new(options)
      end

      def repository
        @repository ||= Repository.new(repository_adapter)
      end

      def repository_adapter
        @repository_adapter ||=
          if options.database&.starts_with?('postgres')
            RepositoryAdapters::Postgres.new
          else
            RepositoryAdapters::Sqlite.new
          end
      end
    end
  end
end
