class CreateRevisions < ActiveRecord::Migration
  def change
    create_table :revisions do |t|
      t.timestamps
    end

    add_reference :revisions, :project, foreign_key: true
  end
end
