class AddDevColumnsToProject < ActiveRecord::Migration
  def change
    add_column(:projects, :dev, :boolean, default: false)
  end
end
