class AddOverridePathColumnsToProject < ActiveRecord::Migration
  def change
    add_column(:projects, :override_path, :string)
  end
end
