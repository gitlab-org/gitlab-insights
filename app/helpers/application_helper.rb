module ApplicationHelper
  def last_updated_string
    source = @project ? @project : @group
    "Last updated: #{source.latest_revision}" if source
  end
end
