Types::IssuableType = GraphQL::ObjectType.define do
  name "Issuable"
  description "An issuable can be an issue or merge request"

  field :issuable_id, !types.Int
  field :iid, !types.Int
  field :project_full_path, !types.String
  field :url, !types.String

  field :author_username, !types.String
  field :throughput, types.String
  field :labels, !types[types.String]
  field :milestone_title, types.String

  field :created_at, GraphQL::Types::ISO8601DateTime
  field :updated_at, GraphQL::Types::ISO8601DateTime
  field :merged_at, GraphQL::Types::ISO8601DateTime
  field :closed_at, GraphQL::Types::ISO8601DateTime

  field :state, !types.String
  field :user_notes_count, !types.Int
  field :confidential, types.Boolean
end
