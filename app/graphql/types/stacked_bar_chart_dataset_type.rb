Types::StackedBarChartDatasetType = GraphQL::ObjectType.define do
  name "StackedBarChartDatasetType"

  field :label, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :data, types[!types.Int] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :backgroundColor, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
