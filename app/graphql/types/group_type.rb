Types::GroupType = GraphQL::ObjectType.define do
  name "Group"
  description "A GitLab project"

  field :path, !types.String
  field :projects, types[Types::ProjectType]

  field :issuables_raw_data, !types[Types::IssuableType],
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::RawDataPresenter,
      GitlabInsights::Finders::RawIssuableFinder
    )

  field :issuables_created_per_month, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::MonthlyCreatedIssuablesPerLabelFinder
    )
  field :cumulative_issuables_created_per_month, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::CumulativeMonthlyCreatedIssuablesPerLabelFinder
    )

  field :issuables_created_per_day, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::DailyCreatedIssuablesPerLabelFinder
    )

  field :issuables_closed_per_month, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::MonthlyClosedIssuablesPerLabelFinder
    )

  field :issuables_per_milestone, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::IssuablesPerMilestoneFinder
    )

  field :issuables_merged_per_month, !Types::StackedBarChartType,
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::MonthlyMergedIssuablesPerLabelFinder
    )

  field :issuables_merged_per_week, !Types::StackedBarChartType,
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::WeeklyMergedIssuablesPerLabelFinder
    )

  field :issuables_closed_per_day, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::DailyClosedIssuablesPerLabelFinder
    )

  field :monthly_issuables_per_state, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::MonthlyIssuablesPerStateFinder
    )

  field :monthly_merge_requests_per_state, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::MonthlyMergeRequestsPerStateFinder
    )

  field :issuables_per_label, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::BarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :issuables_per_label_percentage, !Types::BigNumberPercentType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::BigNumberPercentagePresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :regressions, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::RegressionBarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :missed_deliverables, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::MissedDeliverableBarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :average_issuables_per_milestone, !Types::ComboBarLineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::IssuablesPerMilestonePresenter,
      GitlabInsights::Finders::AverageIssuablesPerMilestoneFinder
    )

  field :average_issuables_merged_per_month, !Types::ComboBarLineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::MrsPerMonthPresenter,
      GitlabInsights::Finders::AverageIssuablesMergedPerMonthFinder
    )

  field :monthly_merged_mrs, !Types::ComboBarLineChartType,
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::Chartjs::MrsPerMonthPresenter,
      GitlabInsights::Finders::MonthlyMergedIssuablesPerStateFinderWithAverage
    )

  field :time_to_close_issues, !Types::ComboBarLineChartType,
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::Chartjs::ComboBarLinePresenter,
      GitlabInsights::Finders::TimeToCloseIssuesFinder
    )
end
