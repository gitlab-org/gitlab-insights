Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  field :group, types.String do
    type Types::GroupType
    argument :group_path, !types.String
    resolve ->(obj, args, ctx) {
      Group.find_by(path: args[:group_path])
    }
  end

  field :project, types.String do
    type Types::ProjectType
    argument :group_path, !types.String
    argument :project_path, !types.String
    resolve ->(obj, args, ctx) {
      Group.find_by(path: args[:group_path]).projects.find_by(path: args[:project_path])
    }
  end
end
