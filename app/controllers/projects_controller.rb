class ProjectsController < ApplicationController
  before_action :find_group
  before_action :find_project, except: [:index]
  before_action :respond_404_for_dev_projects, except: [:index]

  def index
    @projects = @group.projects.listable
  end

  private

  def find_group
    @group = Group.find_by(path: params[:group_id])
  end

  def find_project
    @project = @group.projects.find_by(path: params[:project_id] || params[:id])
  end

  def respond_404_for_dev_projects
    not_found if @project && @project.dev?
  end
end
