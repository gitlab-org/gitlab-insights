class SectionsController < StagesController
  def show
    super

    @team_for_link = @team.dup
    @team.gsub!('_', ' ')
  end
end
