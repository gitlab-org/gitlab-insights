class GroupsController < ApplicationController
  before_action :find_group

  def index
    @groups = Group.all
  end

  def show
    @projects = @group.projects.listable
  end

  def find_group
    @group = Group.find_by(path: params[:group_id] || params[:id])
  end
end
