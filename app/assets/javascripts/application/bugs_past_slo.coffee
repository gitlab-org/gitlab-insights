BugsPastSloChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    @query_strings.BugsPastSlo,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)

CustomerBugsPastSloChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    @query_strings.CustomerBugsPastSlo,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('open-bugs-past-slo')
    BugsPastSloChart(document.getElementById('open-bugs-past-slo'))
  if document.getElementById('open-customer-bugs-past-slo')
    CustomerBugsPastSloChart(document.getElementById('open-customer-bugs-past-slo'))
