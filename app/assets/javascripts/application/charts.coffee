scope = (data) ->
  if data['data']['group']
    'group'
  else
    'project'
transform = (data, view_string) ->
  data_scope = scope(data)
  datasets = if data['data'][data_scope][view_string]['line_dataset'] && data['data'][data_scope][view_string]['bar_dataset']
    [data['data'][data_scope][view_string]['line_dataset'], data['data'][data_scope][view_string]['bar_dataset']].flat()
  else
    data['data'][data_scope][view_string]['datasets']

  {
    labels: data['data'][data_scope][view_string]['labels'],
    datasets: datasets
  }

@charts =
  BarChart: (element, query_string, view_string) ->
    config = {
      type: 'bar',
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    }
    loadChart(element, config, query_string, view_string)
  StackedBarChart: (element, query_string, view_string, disable_toggle = false) ->
    config = {
      type: 'bar',
      options: {
        scales: {
          xAxes: [{
            stacked: true
            }]
          yAxes: [{
            stacked: true
          }]
        },
        tooltips: {
          mode: 'index',
          callbacks: {
            footer: (tooltipItems, ref) ->
              total = tooltipItems
                .map (element) -> element['yLabel']
                .reduce (x, y) -> x + y
              "Total: #{ total }"
          }
        }
      }
    }

    if (disable_toggle)
      config['options']['legend'] = {
        onClick: (e) => e.stopPropagation();
      }

    loadChart(element, config, query_string, view_string)
  LineChart: (element, query_string, view_string) ->
    config = {
      type: 'line',
      options: {
        elements: {
          line: {
            tension: 0,
            fill: false
          }
        },
        scales: {
          yAxes: [{
            ticks: {
                beginAtZero:true
            }
          }]
        }
        tooltips: {
          mode: 'index'
        }
      }
    }
    loadChart(element, config, query_string, view_string)
  PieChart: (element, query_string, view_string) ->
    config = {
      type: 'pie'
    }
    loadChart(element, config, query_string, view_string)
  BigNumberPercentage: (element, query_string, view_string) ->
    $.ajax({
      url: '/graphql',
      method: 'POST',
      dataType: 'json',
      data: { query: query_string },
      success: (data) ->
        data_scope = scope(data)
        data_element = element.querySelector('.total')
        data_element.innerText = data["data"][data_scope]["issuables_per_label_percentage"]["data"] + '%'
        data_element.style.color = data["data"][data_scope]["issuables_per_label_percentage"]["backgroundColor"]
      async: true
    })
  ComboBarLineChart: (element, query_string, view_string, disable_toggle = false) ->
    config = {
      type: 'bar',
      options: {
        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: true
        },
        elements: {
          line: {
            tension: 0
            fill: false
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    }

    if (disable_toggle)
      config['options']['legend'] = {
        onClick: (e) => e.stopPropagation();
      }

    loadChart(element, config, query_string, view_string)

loadChart = (element, chart_config, query_string, view_string) ->
  $.ajax({
    url: '/graphql',
    method: 'POST',
    dataType: 'json',
    data: { query: query_string },
    success: (data) ->
      chart_config['data'] = transform(data, view_string)
      context = element.getContext('2d')
      download_csv_path = $('#download-csv-path').val()
      $(element).parent().prepend(
        "<form action='#{download_csv_path}' method='post'>" +
          "<input type='hidden' value='#{query_string}' name='query'>" +
          "<input type='hidden' value='#{view_string}' name='name'>" +
          "<input type='submit' value='Download CSV'></form>" +
          " (This CSV always includes data within 2 years regardless which charts)"
      )
      chart = new Chart(context, chart_config)
    async: true
  })
