CompletedDeliverablesPerMilestone = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerMilestoneQuery(
    @query_strings.IssuesScope
    'Closed',
    null,
    @query_strings.Deliverable
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('completed-deliverables-per-milestone')
    CompletedDeliverablesPerMilestone(document.getElementById('completed-deliverables-per-milestone'))
