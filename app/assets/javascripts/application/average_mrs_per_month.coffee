AverageMRsMergedPerMonthTeam = (element) ->
  query = @queries.ScopedQuery(@queries.AverageIssuablesMergedPerMonth(
    'Merged',
    @query_strings.MergeRequestsScope,
    true
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)
AverageMRsMergedPerMonthAll = (element) ->
  query = @queries.ScopedQuery(@queries.AverageIssuablesMergedPerMonth(
    'Merged',
    @query_strings.MergeRequestsScope,
    false
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('average-mrs-month-team')
    AverageMRsMergedPerMonthTeam(document.getElementById('average-mrs-month-team'))
  if document.getElementById('average-mrs-month-all')
    AverageMRsMergedPerMonthAll(document.getElementById('average-mrs-month-all'))
