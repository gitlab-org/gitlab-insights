MergedMRsPerMonthWithAvgChart = (element) ->
  query = @queries.ScopedQuery(@queries.MergedMRsPerMonthWithAverageQuery(
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string, true)
MergedMRsPerMonthNoCommChart = (element) ->
  query = @queries.ScopedQuery(@queries.MergedMRsPerMonthWithAverageQuery(
    @query_strings.TeamMRMonths,
    true
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string, true)

$(document).ready () ->
  if document.getElementById('merged-mrs-per-month-with-avg')
    MergedMRsPerMonthWithAvgChart(document.getElementById('merged-mrs-per-month-with-avg'))
  if document.getElementById('merged-mrs-per-month-no-comm')
    MergedMRsPerMonthNoCommChart(document.getElementById('merged-mrs-per-month-no-comm'))
