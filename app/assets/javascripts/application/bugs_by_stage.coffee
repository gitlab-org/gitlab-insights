CumulativeBugsCreatedPerMonthChartStage = (element) ->
  query = @queries.ScopedQuery(@queries.CumulativeIssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Stages,
    @query_strings.IssuesScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

BugsCreatedPerMonthChartStage = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Stages,
    @query_strings.IssuesScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

BugsCreatedPerDayChartStage = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerDayQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Stages,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsClosedPerMonthChartStage = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Stages,
    @query_strings.IssuesScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

BugsClosedPerDayChartStage = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerDayQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Stages,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('cumulative-bugs-created-per-month-stage')
    CumulativeBugsCreatedPerMonthChartStage(document.getElementById('cumulative-bugs-created-per-month-stage'))
  if document.getElementById('bugs-created-per-month-stage')
    BugsCreatedPerMonthChartStage(document.getElementById('bugs-created-per-month-stage'))
  if document.getElementById('bugs-created-per-day-stage')
    BugsCreatedPerDayChartStage(document.getElementById('bugs-created-per-day-stage'))
  if document.getElementById('bugs-closed-per-month-stage')
    BugsClosedPerMonthChartStage(document.getElementById('bugs-closed-per-month-stage'))
  if document.getElementById('bugs-closed-per-day-stage')
    BugsClosedPerDayChartStage(document.getElementById('bugs-closed-per-day-stage'))
