ProposalsCreatedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.FeatureProposals,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

ProposalsClosedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    @query_strings.FeatureProposals,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('proposals-created-per-month')
    ProposalsCreatedPerMonthChart(document.getElementById('proposals-created-per-month'))
  if document.getElementById('proposals-closed-per-month')
    ProposalsClosedPerMonthChart(document.getElementById('proposals-closed-per-month'))
