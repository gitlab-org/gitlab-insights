CumulativeBugsCreatedBySeverityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.CumulativeIssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsCreatedBySeverityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsClosedBySeverityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('cumulative-bugs-created-severity-month')
    CumulativeBugsCreatedBySeverityPerMonthChart(document.getElementById('cumulative-bugs-created-severity-month'))
  if document.getElementById('bugs-created-severity-month')
    BugsCreatedBySeverityPerMonthChart(document.getElementById('bugs-created-severity-month'))
  if document.getElementById('bugs-closed-severity-month')
    BugsClosedBySeverityPerMonthChart(document.getElementById('bugs-closed-severity-month'))
