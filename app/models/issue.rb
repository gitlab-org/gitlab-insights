class Issue < ActiveRecord::Base
  include Issuable

  belongs_to :project

  def age_in_days
    (Date.today - created_at).to_i
  end

  def age_in_months
    age_in_days / 30
  end

  def issuable_id
    issues_id
  end

  def merged_at
  end
end
