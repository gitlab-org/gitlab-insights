class Revision < ActiveRecord::Base
  belongs_to :project

  def to_s
    created_at.strftime('%d %B %Y %H:%M %Z')
  end
end
