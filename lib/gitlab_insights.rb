module GitlabInsights
  COLOR_SCHEME = {
    red: '#e6194B',
    green: '#3cb44b',
    yellow: '#ffe119',
    blue: '#4363d8',
    orange: '#f58231',
    purple: '#911eb4',
    cyan: '#42d4f4',
    magenta: '#f032e6',
    lime: '#bfef45',
    pink: '#fabebe',
    teal: '#469990',
    lavender: '#e6beff',
    brown: '#9A6324',
    beige: '#fffac8',
    maroon: '#800000',
    mint: '#aaffc3',
    olive: '#808000',
    apricot: '#ffd8b1',
    black: '#000000'
  }

  UNCATEGORIZED = 'undefined'
  UNCATEGORIZED_COLOR = "#808080"
  TOP_COLOR = "#FF0000"
  HIGH_COLOR = "#ff8800"
  MEDIUM_COLOR = "#fff600"
  LOW_COLOR = "#008000"
  PROPOSAL_COLOR = "#f0ad4e"
  BUG_COLOR = "#ff0000"
  SECURITY_COLOR = "#d9534f"
  COMMUNITY_CONTRIBUTION_COLOR = "#a8d695"
  DEFAULT_COLOR = "#428bca"
  LINE_COLOR = COLOR_SCHEME[:red]

  AVG_COLOR = "#db4437"
  PERCENTILE_85_COLOR = "#f4b400"
  PERCENTILE_95_COLOR = "#3c78d8"
  CLOSED_ISSUES_COLOR = COLOR_SCHEME[:black]

  STATIC_COLOR_MAP = {
    UNCATEGORIZED => UNCATEGORIZED_COLOR,
    "severity::1" => TOP_COLOR,
    "severity::2" => HIGH_COLOR,
    "severity::3" => MEDIUM_COLOR,
    "severity::4" => LOW_COLOR,
    "priority::1" => TOP_COLOR,
    "priority::2" => HIGH_COLOR,
    "priority::3" => MEDIUM_COLOR,
    "priority::4" => LOW_COLOR,
    "feature" => PROPOSAL_COLOR,
    "bug" => BUG_COLOR,
    "security" => SECURITY_COLOR,
    "Community contribution" => COMMUNITY_CONTRIBUTION_COLOR,
    "backstage [DEPRECATED]" => DEFAULT_COLOR,
    "Quality" => COLOR_SCHEME[:maroon],
    "frontend" => COLOR_SCHEME[:mint],
    'avg' => AVG_COLOR,
    '85' => PERCENTILE_85_COLOR,
    '95' => PERCENTILE_95_COLOR,
    'closed' => CLOSED_ISSUES_COLOR
  }

  ACTIVE_TEAMS = [
    'Delivery',
    'Technical Writing',
    'Quality'
  ]

  # When adding stages or groups please consider updating app/assets/javascripts/application/query_strings.coffee
  DEV_STAGES = {
    'devops::manage': [
      "group::access",
      "group::import",
      "group::analytics",
      "group::compliance"
    ],
    'devops::plan': [
      "group::project management",
      "group::portfolio management",
      "group::certify"
    ],
    'devops::create': [
      "group::source code",
      "group::knowledge",
      "group::editor",
      "group::gitaly",
      "group::gitter"
    ],
    'devops::ecosystem': [
      "group::integrations",
      "group::foundations",
      "group::contributor experience"
    ]
  }

  CICD_STAGES = {
    'devops::verify': [
      "group::continuous integration",
      "group::runner",
      "group::testing"
    ],
    'devops::package': [
      "group::package"
    ],
    'devops::release': [
      "group::core release",
      "group::supporting capabilities"
    ]
  }

  OPS_STAGES = {
    'devops::configure': [
      "group::configure"
    ],
    'devops::monitor': [
      "group::apm",
      "group::health"
    ]
  }

  SECURE_STAGES = {
    'devops::secure': [
      "group::static analysis",
      "group::dynamic analysis",
      "group::composition analysis",
      "group::fuzz testing",
      "group::threat insights",
      "group::vulnerability research"
    ]
  }

  DEFEND_STAGES = {
    'devops::defend': [
      "group::container security",
      "group::insider threat"
    ]
  }

  GROWTH_STAGES = {
    'devops::growth': [
      "group::acquisition",
      "group::conversion",
      "group::expansion",
      "group::retention",
      "group::fulfillment",
      "group::telemetry"
    ]
  }

  ENABLEMENT_STAGES = {
    'devops::enablement': [
      "group::distribution",
      "group::geo",
      "group::memory",
      "group::global search",
      "group::database"
    ]
  }
end
