# frozen_string_literal: true

module GitlabInsights
  module Presenters
    class RawDataPresenter
      def present(result)
        result.preload(project: :group)
      end
    end
  end
end
