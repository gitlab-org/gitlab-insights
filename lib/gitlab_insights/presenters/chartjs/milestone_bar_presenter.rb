module GitlabInsights
  module Presenters
    module Chartjs
      class MilestoneBarPresenter < BarPresenter
        def generate_color_code(label)
          GitlabInsights::DEFAULT_COLOR
        end
      end
    end
  end
end
