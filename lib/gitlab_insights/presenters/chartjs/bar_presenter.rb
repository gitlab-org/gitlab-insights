module GitlabInsights
  module Presenters
    module Chartjs
      class BarPresenter < Presenter
        def present(results)
          labels = []
          result_datasets = Hash.new { |h, k| h[k] = [] }

          results.each do |result|
            result[:elements].each do |name, count|
              labels << name
              result_datasets[:data] << count
              result_datasets[:backgroundColor] << generate_color_code(name)
            end
          end

          chart_data_format(labels, result_datasets)
        end

        private

        # {
        #   labels: ['x', 'y', 'z'],
        #   datasets: [
        #     {
        #       data: ['x', 'y', 'z'],
        #       backgroundColor: ['x', 'y', 'z']
        #     }
        #   ]
        # }
        def chart_data_format(labels, raw_datasets)
          {}.tap do |chart|
            chart[:labels] = labels
            chart[:datasets] = [
              dataset_format(nil, raw_datasets[:data], raw_datasets[:backgroundColor])
            ]
          end.with_indifferent_access
        end

        def dataset_format(label, data, backgroundColor)
          {
            label: label,
            data: data,
            backgroundColor: backgroundColor
          }.with_indifferent_access
        end
      end
    end
  end
end
