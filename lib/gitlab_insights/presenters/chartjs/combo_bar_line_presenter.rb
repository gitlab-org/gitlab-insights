# Combo Bar Line Chart expected format
# Input data

# [
#   {
#     label: 'January',
#     elements: {
#       'bar': [
#         { label: 'Bar 1', count: 2 },
#         { label: 'Bar 2', count: 2 }
#       ],
#       'line': [
#         { label: 'Line 1', count: 0 },
#         { label: 'Line 2', count: 0 }
#       ]
#     }
#   },
#   {
#     label: 'February',
#     elements: {
#       'bar': [
#         { label: 'Bar 1', count: 3 },
#         { label: 'Bar 2', count: 3 }
#       ],
#       'line': [
#         { label: 'Line 1', count: 1 },
#         { label: 'Line 2', count: 1 }
#       ]
#     }
#   },
#   {
#     label: 'March',
#     elements: {
#       'bar': [
#         { label: 'Bar 1', count: 4 },
#         { label: 'Bar 2', count: 4 }
#       ],
#       'line': [
#         { label: 'Line 1', count: 0 },
#         { label: 'Line 2', count: 0 }
#       ]
#     }
#   },
# ]
#
# Outputs
# labels: ['January', 'February', 'March'],
# line_dataset: [{
#   type: 'line',
#   label: 'Line 1',
#   borderColor: window.chartColors.blue,
#   borderWidth: 2,
#   data: [0,1,0]
# }, {
#   type: 'line',
#   label: 'Line 2',
#   borderColor: window.chartColors.green,
#   borderWidth: 2,
#   data: [0,1,0]
# }]
# bar_dataset: [{
#   type: 'bar',
#   label: 'Bar 1',
#   backgroundColor: window.chartColors.red,
#   data: [2,3,4]
# }, {
#   type: 'bar',
#   label: 'Bar 2',
#   backgroundColor: window.chartColors.black,
#   data: [2,3,4]
# }]

module GitlabInsights
  module Presenters
    module Chartjs
      class ComboBarLinePresenter < Presenter
        def present(data)
          labels, lines, bars = [], [], []

          data.each do |datum|
            labels << datum[:label]

            datum[:elements]['line'].each_with_index do |line, index|
              color = default_colors ? line_color : generate_color_code(line[:label])

              active_line = lines[index] ||= build_line_dataset(line[:label], color)

              active_line[:data] << line[:count]
            end

            datum[:elements]['bar'].each_with_index do |bar, index|
              color = default_colors ? bar_color(data, datum) : generate_color_code(bar[:label])

              active_bar = bars[index] ||= build_bar_dataset(bar[:label])

              active_bar[:backgroundColor] << color
              active_bar[:borderColor] << color
              active_bar[:data] << bar[:count]
            end
          end

          {
            labels: labels,
            line_dataset: lines.flatten,
            bar_dataset: bars.flatten
          }.with_indifferent_access
        end

        def default_colors
          false
        end

        private

        def build_line_dataset(label, color)
          {
            type: 'line',
            label: label,
            borderColor: color,
            backgroundColor: '',
            borderWidth: 2,
            data: []
          }
        end

        def build_bar_dataset(label)
          {
            type: 'bar',
            label: label,
            backgroundColor: [],
            borderColor: [],
            data: []
          }
        end
      end
    end
  end
end
