module GitlabInsights
  class DbPopulator
    attr_reader :client, :dev_client

    def initialize(client, dev_client = nil)
      @client, @dev_client = client, dev_client

      if dev_token_necessary? && dev_client.nil?
        raise ArgumentError.new('Dev token required for dev projects')
      end
    end

    def execute!(limit = nil)
      Group.find_each do |group|
        group.projects.find_each do |project|
          project_log(project, 'Retrieving remote project resources', true)

          resources = {
            issues: retrieve_resources(:issues, project, limit),
            merge_requests: retrieve_resources(:merge_requests, project, limit)
          }

          puts
          project_log(project, 'Retrieved remote project resources')
          project_log(project, 'Clearing local project resources', true)

          Issue.delete_all(project: project)
          MergeRequest.delete_all(project: project)

          project_log(project, 'Cleared local project resources')

          add_resources_to_project(project, resources)
          project.revisions.create
        end
      end
    end

    private

    def add_resources_to_project(project, resource_map)
      resource_map.each do |resource_type, resources|
        project_log(project, "Inserting local project #{resource_type}", true)
        resources.each do |resource|
          resource = resource.with_indifferent_access
          resource.delete(:project_id)
          id = resource.delete(:id)
          id_key = "#{resource_type}_id"
          resource = ResourceAnonymizer.new(resource_type, resource).process
          resource = ResourceTranslator.new(resource_type, resource, project: project).process
          resource = ResourceCategorizer.new(resource_type, resource, project: project).process
          project.send(resource_type).create(id_key => id).update(resource)
          print '.'
        end
        puts
        project_log(project, "Inserted local project #{resource_type}", true)
      end
    end

    def project_log(project, message, ongoing = false)
      log = "[[#{Time.now.to_s}]]".tap do |msg|
        msg << ' ' << message
        msg << " for #{project.path}"
        msg << '...' if ongoing
      end
      puts log
    end

    def retrieve_resources(resource, project, limit)
      client = client_for_project(project)
      client.retrieve_resources(resource, project.fetch_path, limit)
    end

    def client_for_project(project)
      project.dev? ? dev_client : client
    end

    def dev_token_necessary?
      Project.where(dev: true).any?
    end
  end
end
