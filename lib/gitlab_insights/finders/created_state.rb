module GitlabInsights
  module Finders
    module CreatedState
      def field
        :created_at
      end
    end
  end
end
