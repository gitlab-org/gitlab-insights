module GitlabInsights
  module Finders
    class PeriodicIssuablesPerStateFinder < IssuablesPerStateFinder
      include LimitPeriod
      include PeriodHelper

      FIELD_TO_STATE_MAP = {
        created_at: 'Opened',
        closed_at: 'Closed',
        merged_at: 'Merged'
      }

      protected

      def group_results(results)
        timestamp_fields.each_with_object({}) do |timestamp_field, map|
          map[timestamp_field] = results_for_attribute_by_period(results, timestamp_field)
        end
      end

      def timestamp_fields
        timestamps = fields.dup
        timestamps.delete(:state)
        timestamps
      end

      def format_results(results, opts)
        formatted_results = period_map(results)
        limit_results(formatted_results, opts[:period_limit])
      end

      def period_map(results_by_period)
        period_keys = results_by_period.map do |k,v|
          v.keys
        end.flatten.uniq.compact.sort

        period_keys.map do |period|
          hash_for_period_count(period, results_by_period)
        end
      end

      def hash_for_period_count(period, results)
        elements = timestamp_fields.each_with_object({}) do |timestamp_field, map|
          count = results[timestamp_field][period] ? results[timestamp_field][period].count : 0
          map[FIELD_TO_STATE_MAP[timestamp_field]] = count
        end

        map = {
          label: period.strftime("%B %Y"),
          elements: elements
        }.with_indifferent_access
      end
    end
  end
end
