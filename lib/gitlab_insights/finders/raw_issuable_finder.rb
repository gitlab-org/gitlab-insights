# frozen_string_literal: true

module GitlabInsights
  module Finders
    class RawIssuableFinder < BaseIssuableFinder
      def find(opts)
        results = super(opts)
        @results = results.where('created_at > ?', 2.years.ago)
      end

      def field
        :created_at
      end
    end
  end
end
