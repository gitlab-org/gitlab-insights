module GitlabInsights
  module Finders
    class MonthlyCreatedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerMonth
      include CreatedState
    end
  end
end
