module GitlabInsights
  module Finders
    class MonthlyClosedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerMonth
      include ClosedState
    end
  end
end
