module GitlabInsights
  module Finders
    class BaseIssuableFinder
      COMMUNITY_LABEL = 'Community contribution'

      attr_reader :project
      attr_reader :presenter
      attr_reader :results

      def initialize(project, presenter, scope)
        @project = project
        @presenter = presenter
        @scope = scope.to_sym
      end

      def present
        presenter.present(@results)
      end

      def find(opts)
        results = project.send(@scope)
        results = results.where(state: opts[:state]) if opts[:state]
        results = results.where("labels @> ARRAY[?]::varchar[]", opts[:filter_labels]) if opts[:filter_labels]
        results = results.order(field => :asc)
        @results = custom_filter_results(results, opts)
      end

      def field
        :iid
      end

      private

      def custom_filter_results(results, opts)
        # Remove core team MRs
        if opts[:exclude_community_contributions]
          remove_community_issuables(results)
        else
          results
        end
      end

      def remove_community_issuables(issuables)
        issuables.where.not("? = ANY (labels)", COMMUNITY_LABEL)
      end
    end
  end
end
