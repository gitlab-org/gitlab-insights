module GitlabInsights
  module Finders
    class AverageIssuablesMergedPerMonthFinder < AverageIssuablesPerMonthFinder
      include MergedState

      def bar_label
        'MRs Merged Per Month'
      end

      def line_label
        'Rolling Average'
      end
    end
  end
end
