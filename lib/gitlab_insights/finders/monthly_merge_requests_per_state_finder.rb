module GitlabInsights
  module Finders
    class MonthlyMergeRequestsPerStateFinder < MonthlyIssuablesPerStateFinder
      include PerMonth

      def fields
        super << :merged_at
      end
    end
  end
end
