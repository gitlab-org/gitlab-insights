module GitlabInsights
  module Finders
    class MonthlyMergedIssuablesPerStateFinder < PeriodicIssuablesPerStateFinder
      include PerMonth
      include MergedState

      def fields
        super << field
      end
    end
  end
end
