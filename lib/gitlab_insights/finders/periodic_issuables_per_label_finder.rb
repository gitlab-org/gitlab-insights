module GitlabInsights
  module Finders
    class PeriodicIssuablesPerLabelFinder < IssuablesPerLabelFinder
      include LimitPeriod
      include PeriodHelper

      protected

      def group_results(results)
        results_for_attribute_by_period(results, field)
      end

      def format_results(results, opts)
        formatted_results = period_map(results, opts[:collection_labels])
        limit_results(formatted_results, opts[:period_limit])
      end

      def period_map(issuables_by_period, labels)
        issuables_by_period.map do |period, issuables|
          hash_for_period_count(period, issuables, labels) if period
        end.compact
      end

      def hash_for_period_count(period, period_issuables, labels)
        {
          label: period.strftime(period_format),
          elements: issuables_with_label_count(period_issuables, labels)
        }.with_indifferent_access
      end
    end
  end
end
