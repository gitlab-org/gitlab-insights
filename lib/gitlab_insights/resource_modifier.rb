module GitlabInsights
  class ResourceModifier
    APPLICABLE_TYPES = [
      :issues,
      :merge_requests
    ].freeze

    def initialize(resource_type, resource, project: nil)
      @resource_type = resource_type
      @resource = resource.dup
      @project = project
    end

    def process
      return unless applicable?

      do_process

      resource
    end

    private

    attr_reader :resource_type, :resource, :project

    def applicable?
      APPLICABLE_TYPES.include?(resource_type)
    end

    def do_process
      raise NotImplementedError
    end
  end
end
