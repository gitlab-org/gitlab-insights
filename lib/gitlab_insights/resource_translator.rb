module GitlabInsights
  class ResourceTranslator < ResourceModifier
    def initialize(resource_type, resource, project:)
      super
    end

    private

    def do_process
      translate_username
    end

    def translate_username
      return unless project.dev?
      return unless DevUsernames::DEV_USERNAME_MAPPING.key?(resource[:author][:username])

      resource[:author][:username] = DevUsernames::DEV_USERNAME_MAPPING[resource[:author][:username]]
    end
  end
end
