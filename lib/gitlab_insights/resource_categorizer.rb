module GitlabInsights
  class ResourceCategorizer < ResourceModifier
    def initialize(resource_type, resource, project:)
      super
    end

    private

    def do_process
      add_security_label
    end

    def add_security_label
      return unless project.dev?
      return unless resource_type == :merge_requests

      resource[:labels] << 'security' unless resource[:labels].include?('security')
    end
  end
end
